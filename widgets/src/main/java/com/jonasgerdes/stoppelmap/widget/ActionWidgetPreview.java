package com.jonasgerdes.stoppelmap.widget;


import androidx.annotation.IdRes;

/**
 * Created by jonas on 09.03.2017.
 */
public interface ActionWidgetPreview {
    void setAction(@IdRes int actionId);
}
