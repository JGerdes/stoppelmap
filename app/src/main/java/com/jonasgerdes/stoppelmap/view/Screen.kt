package com.jonasgerdes.stoppelmap.view

enum class Screen {
    Home,
    Map,
    Schedule,
    Transport,
    News,
    About
}