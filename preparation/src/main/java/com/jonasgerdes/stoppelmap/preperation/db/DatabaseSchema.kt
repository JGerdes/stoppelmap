package com.jonasgerdes.stoppelmap.preperation.db

data class DatabaseSchema(
    val formatVersion: Int,
    val database: Database
)