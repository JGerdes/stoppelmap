package com.jonasgerdes.stoppelmap.preperation.entity

val marqueMappings = mapOf(
    "zelte/alkoholfreies-zelt/" to "alkoholfreies-zelt-katholischer-frauenbind",
    "zelte/anton-linnemann/" to "anton-linneman-linneman-gbr",
    "zelte/beckmanns-alter-dorfplatz/" to "altdeutscher-biergarten-beckmann",
    "zelte/brackmann/" to "brackmann-meckl",
    "zelte/hogebacks-festzelt/" to "hogeback-muhle",
    "zelte/kaponierzelt/" to "kaponier-zjzhzm",
    "zelte/kuehlings-niedersachsenhalle/" to "kuehlings-niedersachsenhalle",
    "zelte/party-park/" to "party-park-von-doellen-hake",
    "zelte/pickers-lutten-das-partyzelt/" to "pickers-surrmann",
    "zelte/stratmanns-echt-stoppelmarkt/" to "stratmann",
    "zelte/wein-ellendorff/" to "ellendorf",
    "zelte/wunderbar-zelt/" to "waldhof-grieshop",
    "zelte/zauberbar/" to "zauberbar-v-doellen-hohnhorst",
    "zelte/zum-alten-pferdestall/" to "alter-pferdestall-tepe",
    "zelte/zur-traenke/" to "zur-traenke-suing-k-"
)