package com.jonasgerdes.stoppelmap.about.data

import com.jonasgerdes.stoppelmap.about.view.AuthorCard

internal val sources = listOf(
    AuthorCard(
        name = "Stadt Vechta",
        work = "Lageplan, Infotexte, Programm, Fahrpläne",
        website = "https://stoppelmarkt.de"
    ),
    AuthorCard(
        name = "Schaustellerverein Vechta",
        work = "Informationen",
        website = "https://schaustellerverein-vechta.de/de/pages/show/freizeitparkvechta"
    ),
    AuthorCard(
        name = "Stoppelmarkt/Stadt Vechta",
        work = "Newsfeed",
        website = "https://stoppelmarkt.de/aktuelles"
    ),
    AuthorCard(
        name = "Schaustellerverein Vechta",
        work = "Newsfeed",
        website = "https://schaustellerverein-vechta.de/de/pages/show/aktuelles"
    )
)