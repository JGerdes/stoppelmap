package com.jonasgerdes.stoppelmap.about.data

import com.jonasgerdes.stoppelmap.about.view.AuthorCard

val illustrators = listOf(
    AuthorCard(
        name = "Sara Fraas",
        work = "Süßigkeiten, Dosenwerfen",
        website = "http://fraas-home.de"
    ),
    AuthorCard(
        name = "Jonas Gerdes",
        work = "Alle nicht weiter gekennzeichneten Grafiken und Fotos",
        mail = "moin@jonasgerdes.com",
        website = "https://jonasgerdes.com",
        githubUrl = "https://github.com/JGerdes"
    ),
    AuthorCard(
        name = "Jordan Steranka",
        work = "Foto: Feuerwerk",
        website = "https://unsplash.com/@jordansteranka"
    )
)