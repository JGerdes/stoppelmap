package com.jonasgerdes.stoppelmap.about.data

import com.jonasgerdes.stoppelmap.about.view.AuthorCard

internal val contributors = listOf(
    AuthorCard(
        name = "Jonas Gerdes",
        work = "Idee, Design, Programmierung",
        mail = "moin@jonasgerdes.com",
        website = "https://jonasgerdes.com",
        githubUrl = "https://github.com/JGerdes",
        gitlabUrl = "https://gitlab.com/JGerdes"
    )
)