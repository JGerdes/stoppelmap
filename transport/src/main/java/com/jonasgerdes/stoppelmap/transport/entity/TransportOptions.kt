package com.jonasgerdes.stoppelmap.transport.entity

data class TransportOptions(
    val bus: List<BusRoute>?
)