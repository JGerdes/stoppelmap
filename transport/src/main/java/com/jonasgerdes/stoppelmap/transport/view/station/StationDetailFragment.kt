package com.jonasgerdes.stoppelmap.transport.view.station

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.jonasgerdes.androidutil.view.consumeWindowInsetsTop
import com.jonasgerdes.stoppelmap.core.di.viewModelFactory
import com.jonasgerdes.stoppelmap.core.routing.Router
import com.jonasgerdes.stoppelmap.core.util.observe
import com.jonasgerdes.stoppelmap.transport.R
import kotlinx.android.synthetic.main.fragment_option_list.toolbar
import kotlinx.android.synthetic.main.fragment_station_detail.*

class StationDetailFragment : Fragment(R.layout.fragment_station_detail) {

    val departureAdapter = DepartureAdapter()

    val title: String? by lazy { arguments!!.getString(ARGUMENT_TITLE) }
    val subtitle: String? by lazy { arguments!!.getString(ARGUMENT_SUBTITLE) }
    val slug: String by lazy { arguments!!.getString(ARGUMENT_SLUG)!! }

    val viewModel: StationDetailViewModel by viewModelFactory()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.consumeWindowInsetsTop()
        if (title != null) toolbar.title = title
        if (subtitle != null) toolbar.subtitle = subtitle
        toolbar.setNavigationOnClickListener {
            Router.navigateBack()
        }

        departureGrid.adapter = departureAdapter
        departureGrid.itemAnimator = null
        (departureGrid.layoutManager as GridLayoutManager).spanSizeLookup =
            object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int) = departureAdapter.items[position].getSpanSize()
            }

        viewModel.setStation(slug)

        observe(viewModel.station) { station ->
            prices.text = station.prices.joinToString("\n") {
                val price = "%.2f€".format(it.price / 100.0)
                "${it.type}: $price"
            }

            prices.isVisible = station.prices.isNotEmpty()
            priceLabel.isVisible = station.prices.isNotEmpty()

            departureAdapter.submitList(station.departures.flatMap { slot ->
                listOf(
                    DepartureGridItem.TimeSpanHeader(slot.type)
                ) + slot.slots
                    .filter { it.departures.isNotEmpty() }
                    .flatMap {
                        val maxSize = it.departures.map { it.departures.size }.sortedDescending().first()
                        (0 until maxSize).flatMap { i ->
                            it.departures.map {
                                it.departures.getOrNull(i)?.let { DepartureGridItem.Departure(it) }
                                    ?: DepartureGridItem.Empty
                            }
                        }
                    }
            })
        }
    }

    companion object {
        private const val ARGUMENT_SLUG = "argument_slug"
        private const val ARGUMENT_TITLE = "argument_title"
        private const val ARGUMENT_SUBTITLE = "argument_subtitle"
        fun newInstance(slug: String, title: String? = null, subtitle: String? = null) =
            StationDetailFragment().apply {
                arguments = bundleOf(
                    ARGUMENT_SLUG to slug,
                    ARGUMENT_TITLE to title,
                    ARGUMENT_SUBTITLE to subtitle
                )
            }
    }
}