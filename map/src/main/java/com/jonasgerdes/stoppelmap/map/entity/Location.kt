package com.jonasgerdes.stoppelmap.map.entity

data class Location(
    val longitude: Double,
    val latitude: Double
)

