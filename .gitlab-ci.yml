stages:
  - environment
  - test
  - build
  - internal
  - beta

.updateContainerJob:
  image: docker:stable
  stage: environment
  services:
    - docker:dind
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG || true
    - docker build --cache-from $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG -t $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

#update docker container if Dockerfile is changed
updateContainer:
  extends: .updateContainerJob
  only:
    changes:
      - Dockerfile

#create a docker container if there isn't one
#for cases where updateContainer fails and it isn't retriggered this prevents a branch without a
#container)
ensureContainer:
  extends: .updateContainerJob
  allow_failure: true
  before_script:
    - "mkdir -p ~/.docker && echo '{\"experimental\": \"enabled\"}' > ~/.docker/config.json"
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    # Skip update container `script` if the container already exists
    # via https://gitlab.com/gitlab-org/gitlab-ce/issues/26866#note_97609397 -> https://stackoverflow.com/a/52077071/796832
    - docker manifest inspect $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG > /dev/null && exit || true



.testJob:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  stage: test
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradle
    - chmod +x ./gradlew
    - echo $GOOGLE_SERVICES_JSON | base64 -d > ./app/google-services.json
  after_script:
    - rm ./app/google-services.json
  cache:
    key: "$CI_COMMIT_REF_NAME"
    paths:
      - .gradle/

unitTest:
  extends: .testJob
  script:
    - ./gradlew testDebugUnitTest
  artifacts:
    paths:
      - ./**/build/reports/tests/
    reports:
      junit: ./**/build/test-results/**/TEST-*.xml


.buildJob:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  stage: build
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradle
    - chmod +x ./gradlew
    - echo $GOOGLE_SERVICES_JSON | base64 -d > ./app/google-services.json
    - base64 -d -i $RELEASE_KEYSTORE > ./ci_release.keystore
    - echo "keystorePath=../ci_release.keystore" > ./signing.properties
    - echo "keystorePassword=$RELEASE_STORE_PASSWORD" >> ./signing.properties
    - echo "keyAlias=$RELEASE_KEY_ALIAS" >> ./signing.properties
    - echo "keyPassword=$RELEASE_KEY_PASSWORD" >> ./signing.properties
  after_script:
    - rm ./app/google-services.json
    - rm ./ci_release.keystore
    - rm ./signing.properties

  cache:
    key: "$CI_COMMIT_REF_NAME"
    paths:
      - .gradle/

bundleRelease:
  extends: .buildJob
  script:
    - ./gradlew preparation:runPreparation
    - ./gradlew bundleRelease
  artifacts:
    paths:
      - ./app/build/outputs/bundle/release/

buildReleaseApk:
  extends: .buildJob
  when: manual
  script:
    - ./gradlew preparation:runPreparation
    - ./gradlew assembleRelease
  artifacts:
    paths:
      - ./app/build/outputs/apk/release/

publishInternal:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  stage: internal
  when: manual
  before_script:
    - chmod +x ./gradlew
    - export GRADLE_USER_HOME=`pwd`/.gradle
    - echo $GOOGLE_SERVICES_JSON | base64 -d > ./app/google-services.json
    - base64 -d -i $RELEASE_KEYSTORE > ./ci_release.keystore
    - echo "keystorePath=../ci_release.keystore" > ./signing.properties
    - echo "keystorePassword=$RELEASE_STORE_PASSWORD" >> ./signing.properties
    - echo "keyAlias=$RELEASE_KEY_ALIAS" >> ./signing.properties
    - echo "keyPassword=$RELEASE_KEY_PASSWORD" >> ./signing.properties
    - base64 -d -i $GOOGLE_PLAY_API_KEY > ./google_play_api_key.json
  after_script:
    - rm ./google_play_api_key.json
    - rm ./app/google-services.json
    - rm ./signing.properties
  script:
    - bundle exec fastlane internal

promoteToBeta:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  stage: beta
  when: manual
  script:
    - bundle exec fastlane promote_internal_to_beta