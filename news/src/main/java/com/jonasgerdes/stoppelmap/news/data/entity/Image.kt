package com.jonasgerdes.stoppelmap.news.data.entity

data class Image(
    val url: String,
    val author: String?,
    val caption: String?
)