package com.jonasgerdes.stoppelmap.model.map

enum class Type(val type: String) {
    BAR("bar"),
    BUILDING("building"),
    CANDY_STALL("candy-stall"),
    EXHIBITION("exhibition"),
    FOOD_STALL("food-stall"),
    GAME_STALL("game-stall"),
    MISC("misc"),
    RESTROOM("restroom"),
    RIDE("ride"),
    SELLER_STALL("seller-stall")
}